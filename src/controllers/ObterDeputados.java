/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import java.io.IOException;
import java.util.List;
import javax.xml.bind.JAXBException;

/**
 *
 * @author joao-sconetto
 */
public class ObterDeputados implements Runnable {
    private Camara deputados;
    private List<Deputado> deputadoList;
    
    public ObterDeputados() {
        deputados = new Camara();
        deputadoList = null;
        this.run();
        deputadoList = deputados.getDeputados();
    }

    @Override
    public void run() {
       try{
           deputados.obterDados();
       }
       catch(JAXBException jaxbException){
       }
       catch(IOException camaraException){
       }
    }
}

    
